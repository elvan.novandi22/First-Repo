package hello;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = GreetingController.class)

public class ApplicationTest {
    private Class<?> application;
    private Class<?> greeting;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        application = Class.forName("hello.Application");
        greeting = Class.forName("hello.GreetingController");
    }


    @Test
    public void testMain() {
        Application.main(new String[]{
            "--spring.main.web-environment=false",
            "--spring.autoconfigure.exclude=truellu",
        });
        int classModifiers = application.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));

    }

    @Test
    public void homePage() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        int classModifiers = greeting.getModifiers();
        mockMvc.perform(get("/index.html"))
                .andExpect(content().string(containsString("Lihat CV")));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void greetingWithUser() throws Exception {
        mockMvc.perform(get("/greeting").param("name", "Greg"))
                .andExpect(content().string(
                    containsString("Greg, I hope you are interested to hire me")));
        mockMvc.perform(get("/greeting").param("name", "Greg"))
                .andExpect(content().string(containsString("Hello, Greg!")));
    }

    @Test
    public void greetingAnonymous() throws Exception {
        mockMvc.perform(get("/greeting").param("name", ""))
                .andExpect(content().string(containsString("This is my CV")));
        mockMvc.perform(get("/greeting").param("name", ""))
                .andExpect(content().string(containsString("Hello, Anonymous!")));
    }
}
