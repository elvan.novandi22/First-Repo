package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    public SecurityExpert(String name, double salary) {
        //TODO Implement
    	super(name, salary);
    	this.role="Security Expert";
    	if(this.salary<70000.00) {
    		throw new IllegalArgumentException();
    	}
    }

    @Override
    public double getSalary() {
        //TODO Implement
    	return this.salary;
    }
}
