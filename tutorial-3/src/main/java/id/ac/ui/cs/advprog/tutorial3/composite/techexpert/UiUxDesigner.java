package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, double salary) {
        //TODO Implement
    	super(name, salary);
    	this.role="UI/UX Designer";
    	if(this.salary<90000.00) {
    		throw new IllegalArgumentException();
    	}
    }

    @Override
    public double getSalary() {
        //TODO Implement
    	return this.salary;
    }
}
