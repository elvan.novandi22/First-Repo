package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {
    public BackendProgrammer(String name, double salary) {
        //TODO Implement
    	super(name, salary);
    	this.role="Back End Programmer";
    	if(this.salary<20000.00) {
    		throw new IllegalArgumentException();
    	}
    }

    @Override
    public double getSalary() {
        //TODO Implement
    	return this.salary;
    }
}
