package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) {
        //TODO Implement
    	super(name, salary);
    	this.role="Network Expert";
    	if(this.salary<50000.00) {
    		throw new IllegalArgumentException();
    	}
    }

    @Override
    public double getSalary() {
        //TODO Implement
    	return this.salary;
    }
}
