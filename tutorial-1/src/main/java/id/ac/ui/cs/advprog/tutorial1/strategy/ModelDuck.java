package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{

	public ModelDuck() {
		setFlyBehavior(new FlyNoWay());
		setQuackBehavior(new MuteQuack());
		// TODO Auto-generated constructor stub
	}
	public void display() {
		System.out.println("model duck");
	}
    // TODO Complete me!
	
	
}
