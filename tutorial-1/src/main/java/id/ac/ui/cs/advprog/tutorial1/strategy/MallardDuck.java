package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {

	public MallardDuck() {
		// TODO Auto-generated constructor stub
		setFlyBehavior(new FlyWithWings());
		setQuackBehavior(new Quack());
	}
	public void display() {
		System.out.println("mallard duck");
	}
	
    // TODO Complete me!
}
