package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {

    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings=0;

    public StatisticsDisplay(Observable observable) {
        // TODO Complete me!
    	observable.addObserver(this);
    	
    }

    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }

    @Override
    public void update(Observable o, Object arg) {
    	 if (o instanceof WeatherData) {
             if(arg instanceof ArrayList<?>) {
             	ArrayList<Float> arr= (ArrayList<Float>) arg;
             	Float newTemp= arr.get(0);
             	numReadings+=1;
             	if(minTemp==200) {
             		minTemp=newTemp;
             	}
             	if(newTemp<minTemp) {
             		this.minTemp=newTemp;
             	}
             	else if(newTemp>minTemp) {
             		this.maxTemp=newTemp;
             	}
             	this.tempSum+=newTemp;
             	display();
             	
             }
         }
    }
}
