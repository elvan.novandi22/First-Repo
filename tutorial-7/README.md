CREATE SCHEMA SIRESTO;

SET search_path to SIRESTO;

SET datestyle = "ISO, DMY";

CREATE TABLE KARYAWAN(
id CHAR(5),
nama VARCHAR(50) NOT NULL,
tgl_lahir DATE,
no_KTP CHAR(16) NOT NULL,
posisi VARCHAR(18) NOT NULL,
UNIQUE (no_KTP),
PRIMARY KEY (id));

CREATE TABLE KOKI(
id CHAR(5),
sertifikasi VARCHAR(50) NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (id) REFERENCES KARYAWAN (id) ON
UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE SUPPLIER(
id CHAR(5),
nama VARCHAR(50) NOT NULL,
telepon VARCHAR(15) NOT NULL,
email TEXT,
PRIMARY KEY (id));

CREATE TABLE KATEGORI(
id CHAR(5),
nama VARCHAR(50) NOT NULL,
PRIMARY KEY (id));

CREATE TABLE MENU(
nama VARCHAR(50),
harga INT NOT NULL,
id_kategori CHAR(5) NOT NULL,
PRIMARY KEY (nama),
FOREIGN KEY (id_kategori) REFERENCES KATEGORI (id)
ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE MENU_HARIAN(
nama_menu VARCHAR(50),
tanggal DATE,
id_koki CHAR(5),
jumlah SMALLINT NOT NULL,
PRIMARY KEY (nama_menu, tanggal, id_koki),
FOREIGN KEY (nama_menu) REFERENCES MENU (nama)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (id_koki) REFERENCES KOKI (id)
ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE BAHAN_BAKU(
nama VARCHAR(50),
stok SMALLINT NOT NULL,
satuan VARCHAR(10) NOT NULL,
PRIMARY KEY (nama));

CREATE TABLE BAHAN_BAKU_MENU(
nama_menu VARCHAR(50),
nama_bahan VARCHAR(50),
jumlah SMALLINT NOT NULL,
satuan VARCHAR(10) NOT NULL,
PRIMARY KEY (nama_menu, nama_bahan),
FOREIGN KEY (nama_menu) REFERENCES MENU (nama)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (nama_bahan) REFERENCES BAHAN_BAKU (nama)
ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE PEMBELIAN_BAHAN_BAKU(
nota VARCHAR(10),
nama_bahan VARCHAR(50) NOT NULL,
jumlah SMALLINT NOT NULL,
satuan VARCHAR(10) NOT NULL,
harga_satuan INT NOT NULL,
WAKTU DATE NOT NULL,
id_supplier CHAR(5),
id_karyawan CHAR(5),
PRIMARY KEY (nota),
FOREIGN KEY (nama_bahan) REFERENCES BAHAN_BAKU (nama)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (id_supplier) REFERENCES SUPPLIER (id)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (id_karyawan) REFERENCES KARYAWAN (id)
ON UPDATE CASCADE ON DELETE CASCADE);

INPUT DATA KARYAWAN:

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16901', 'Alan Nolan', '1979-02-19',
'7867313192486378', 'manager');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16902', 'Adi Stone', '1981-07-06',
'3958957716354255', 'koki');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16903', 'Benadict Goff', '1989-01-07',
'5138557373336434', 'koki');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16904', 'Benjamin Hadi', '1990-06-11',
'8654380767277662', 'koki');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16905', 'Cassandra Bishop', '1987-07-06',
'9810599846936552', 'staff');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16906', 'Charles Talley', '1986-11-21',
'1742870859287685', 'staff');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16907', 'Charles Walters', '1992-05-24',
'6101796466188583', 'staff');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16908', 'Curran Tradi', '1981-06-22',
'2571924567963357', 'cashier');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16909', 'Darrel Owen', '1992-06-07',
'9045108692648731', 'koki');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16910', 'Evan Fleming', '1974-07-25',
'2188923777981912', 'cashier');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16911', 'Fiona Moran', '1977-07-27',
'9551219455894049', 'staff');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16912', 'Hermione Ferguson', '1971-05-10',
'6706949116938119', 'koki');

INSERT INTO KARYAWAN(
id, nama, tgl_lahir, no_KTP, posisi)
VALUES(
'16913', 'Hilary Bradley', '1973-09-11',
'9732119515542017', 'koki');

INPUT DATA KOKI:

INSERT INTO KOKI(id, sertifikasi)
VALUES('16902', 'Pastry');

INSERT INTO KOKI(id, sertifikasi)
VALUES('16903', 'Fry');

INSERT INTO KOKI(id, sertifikasi)
VALUES('16904', 'Pastry');

INSERT INTO KOKI(id, sertifikasi)
VALUES('16909', 'Fry');

INSERT INTO KOKI(id, sertifikasi)
VALUES('16912', 'Grill');

INSERT INTO KOKI(id, sertifikasi)
VALUES('16913', 'Pastry');

INPUT DATA SUPPLIER:

INSERT INTO SUPPLIER(
id, nama, telepon)
VALUES('10001', 'Airawati Shop',
'087865764567');

INSERT INTO SUPPLIER(
id, nama, telepon, email)
VALUES('10002', 'Ali Sumihar Shop',
'087656434321', 'AliSumi@yahoo.com');

INSERT INTO SUPPLIER(
id, nama, telepon)
VALUES('10003', 'Bagas Aras Shop',
'081234565432');

INSERT INTO SUPPLIER(
id, nama, telepon, email)
VALUES('10004', 'Bertua Shop',
'081234567865', 'Bertuasss@gmail.com');

INSERT INTO SUPPLIER(
id, nama, telepon, email)
VALUES('10005', 'Chintya Shop',
'081234567654', 'Chintya25@yahoo.com');

INSERT INTO SUPPLIER(
id, nama, telepon, email)
VALUES('10006', 'Dame Damanik Shop',
'081245768932', 'Dame.damanik@yahoo.com');

INSERT INTO SUPPLIER(
id, nama, telepon, email)
VALUES('10007', 'Dame Uli Shop',
'081234568211', 'Dameuli@gmail.com');

INSERT INTO SUPPLIER(
id, nama, telepon, email)
VALUES('10008', 'Emil Sutrisno Shop',
'082456784356', 'EmilSutrisno@yahoo.com');

INSERT INTO SUPPLIER(
id, nama, telepon, email)
VALUES('10009', 'Ifan Shop',
'087654567876', 'ifan@gmail.com');

INSERT INTO SUPPLIER(
id, nama, telepon)
VALUES('10010', 'Igun Ranaro Shop',
'085678415678');

INSERT INTO SUPPLIER(
id, nama, telepon, email)
VALUES('10011', 'Joni Shop',
'08765467876', 'joni@gmail.com');

INPUT DATA KATEGORI:

INSERT INTO KATEGORI(id, nama)
VALUES('33331', 'Makanan');

INSERT INTO KATEGORI(id, nama)
VALUES('33332', 'Minuman');

INSERT INTO KATEGORI(id, nama)
VALUES('33333', 'Cemilan');

INSERT INTO KATEGORI(id, nama)
VALUES('33334', 'Pencuci Mulut');

INPUT DATA MENU:

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Air mineral', 5000, '33332');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Ayam goreng tepung', 30000, '33331');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Bebek goreng mentega', 30000, '33331');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Bubur ayam', 20000, '33331');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Cah kailan', 25000, '33331');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Cumi bakar', 30000, '33333');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Cumi goreng', 30000, '33333');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Es teh', 3000, '33332');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Es teh manis', 5000, '33332');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Gurame bakar', 28000, '33331');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Gurame goreng asam manis',
28000, '33331');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Jus alpukat', 15000, '33332');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Jus apel', 15000, '33332');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Kue keju', 25000, '33334');

INSERT INTO MENU(nama, harga, id_kategori)
VALUES('Kue tiramisu',30000, '33334');

INPUT DATA MENU_HARIAN:

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Air mineral', '2016-07-04',
'16912', 2);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Air mineral', '2016-07-04',
'16903', 5);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cumi goreng', '2016-07-05',
'16902', 2);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh', '2016-07-05',
'16903', 5);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cah kailan', '2016-07-05',
'16902', 3);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Jus alpukat', '2016-07-06',
'16904', 3);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cumi bakar', '2016-07-07',
'16912', 7);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Gurame goreng asam manis',
'2016-07-10', '16912', 4);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Kue keju', '2016-07-10',
'16909', 1);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cah kailan', '2016-07-10',
'16904', 6);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh', '2016-07-11',
'16904', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Gurame goreng asam manis',
'2016-07-12', '16902', 5);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Jus alpukat', '2016-07-12',
'16909', 3);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Jus alpukat', '2016-07-13',
'16903', 3);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Ayam goreng tepung', '2016-07-17',
'16909', 8);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bebek goreng mentega', '2016-07-17',
'16912', 5);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cah kailan', '2016-07-17',
'16904', 3);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Kue keju', '2016-07-18',
'16909', 7);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cumi goreng', '2016-07-19',
'16902', 6);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh', '2016-07-20',
'16902', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Kue keju', '2016-07-24',
'16904', 6);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Kue keju', '2016-07-25',
'16902', 8);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Jus alpukat', '2016-07-26',
'16913', 1);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Kue keju', '2016-07-26',
'16902', 2);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Kue keju', '2016-07-26',
'16903', 4);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cumi goreng', '2016-07-26',
'16904', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh', '2016-07-27',
'16902', 5);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh manis', '2016-07-31',
'16913', 5);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Gurame bakar', '2016-08-01',
'16909', 8);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cumi goreng', '2016-08-01',
'16904', 9);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Kue keju', '2016-08-01',
'16902', 9);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bubur ayam', '2016-08-02',
'16909', 6);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Kue tiramisu', '2016-08-03',
'16902', 8);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bebek goreng mentega', '2016-08-03',
'16904', 8);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bubur ayam', '2016-08-07',
'16902', 7);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh manis', '2016-08-07',
'16904', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Gurame bakar', '2016-08-07',
'16913', 3);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh', '2016-08-07',
'16909', 8);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Gurame bakar', '2016-08-07',
'16902', 8);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh', '2016-08-08',
'16904', 1);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bebek goreng mentega', '2016-08-09',
'16912', 5);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bubur ayam', '2016-08-14',
'16903', 6);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Kue tiramisu', '2016-08-14',
'16909', 8);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh manis', '2016-08-14',
'16913', 8);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Jus apel', '2016-08-15',
'16912', 7);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Kue tiramisu', '2016-08-16',
'16902', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bebek goreng mentega', '2016-08-16',
'16903', 3);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh', '2016-08-16',
'16904', 8);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bubur ayam', '2016-08-21',
'16909', 4);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Gurame bakar', '2016-08-22',
'16903', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cah kailan', '2016-08-22',
'16902', 5);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh', '2016-08-22',
'16904', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Gurame bakar', '2016-08-23',
'16903', 6);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bubur ayam', '2016-08-23',
'16903', 7);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cah kailan', '2016-08-24',
'16904', 2);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bebek goreng mentega', '2016-08-28',
'16902', 4);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cah kailan', '2016-08-28',
'16909', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bebek goreng mentega', '2016-08-28',
'16904', 5);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh manis', '2016-08-28',
'16913', 3);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Kue tiramisu', '2016-08-28',
'16902', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bebek goreng mentega', '2016-08-28',
'16903', 3);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh', '2016-08-29',
'16904', 8);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bubur ayam', '2016-08-29',
'16909', 4);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Gurame bakar', '2016-08-29',
'16903', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cah kailan', '2016-08-30',
'16902', 5);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Gurame bakar', '2016-08-31',
'16903', 6);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Es teh', '2016-09-01',
'16904', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bubur ayam', '2016-09-02',
'16903', 7);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cah kailan', '2016-09-02',
'16904', 2);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bebek goreng mentega', '2016-09-02',
'16902', 4);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Cah kailan', '2016-09-03',
'16909', 10);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Bebek goreng mentega', '2016-09-04',
'16904', 5);

INSERT INTO MENU_HARIAN(nama_menu, tanggal,
id_koki, jumlah)
VALUES('Air mineral', '2016-09-05',
'16913', 3);

INPUT DATA BAHAN_BAKU_MENU:

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Air mineral', 'Air', 300, 'ml');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Ayam goreng tepung',
'Ayam', 1, 'ekor');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Ayam goreng tepung',
'Tepung', 100, 'gram');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Ayam goreng tepung',
'Garam', 20, 'gram');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Bebek goreng mentega',
'Bebek', 1, 'ekor');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Bebek goreng mentega',
'Garam', 25, 'gram');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Bubur ayam',
'Ayam', 1, 'ekor');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Bubur ayam',
'Beras', 200, 'gram');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Cah kailan',
'Kailan', 1, 'ikat');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Cah kailan',
'Bawang putih', 1, 'siung');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Cah kailan',
'Garam', 5, 'gram');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Cumi bakar',
'Cumi', 3, 'ons');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Cumi goreng',
'Cumi', 4, 'ons');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Cumi goreng',
'Tepung', 50, 'gram');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Es teh',
'Teh', 1, 'kantong');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Es teh',
'Air', 400, 'ml');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Es teh manis',
'Teh', 1, 'kantong');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Es teh manis',
'Air', 400, 'ml');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Es teh manis',
'Gula', 5, 'gram');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Gurame bakar',
'Gurame', 1, 'ekor');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Gurame goreng asam manis',
'Gurame', 1, 'ekor');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Gurame goreng asam manis',
'Gula', 10, 'gram');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Jus alpukat',
'Alpukat', 1, 'buah');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Jus alpukat',
'Air', 100, 'ml');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Jus apel',
'Apel', 2, 'buah');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Jus apel',
'Air', 50, 'ml');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Kue keju',
'Telur', 3, 'buah');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Kue keju',
'Keju', 250, 'gram');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Kue tiramisu',
'Telur', 3, 'buah');

INSERT INTO BAHAN_BAKU_MENU(nama_menu,
nama_bahan, jumlah, satuan)
VALUES('Kue tiramisu',
'Kopi', 5, 'gram');

INPUT DATA BAHAN_BAKU:

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Air', 6000, 'ml');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Ayam', 30, 'ekor');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Bebek', 26, 'ekor');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Tepung', 10000, 'gram');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Cumi', 50, 'ons');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Telur', 63, 'butir');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Teh', 123, 'kantong');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Gurame', 48, 'ekor');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Alpukat', 57, 'buah');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Apel', 36, 'buah');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Keju', 2360, 'gram');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Kopi', 1948, 'gram');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Kailan', 67, 'ikat');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Bawang putih', 6, 'siung');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Gula', 3560, 'gram');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Beras', 1250, 'gram');

INSERT INTO BAHAN_BAKU(nama, stok, satuan)
VALUES('Garam', 5329, 'gram');

INPUT DATA PEMBELIAN_BAHAN_BAKU:

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('78452', 'Tepung', 2500,
'gram', 8, '2016-07-01', '10011',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('75696', 'Teh', 10,
'kantong', 1500, '2016-07-01', '10008',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('77429', 'Kailan', 10,
'ikat', 2900, '2016-07-01', '10011',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('75011', 'Kailan', 10,
'ikat', 3000, '2016-07-08', '10011',
'16906');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('78302', 'Gurame', 2,
'ekor', 15000, '2016-07-08', '10009',
'16907');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('79221', 'Garam', 1000,
'gram', 50, '2016-07-08', '10008',
'16911');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('73576', 'Gula', 1000,
'gram', 50, '2016-07-08', '10008',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('74233', 'Teh', 15,
'kantong', 1500, '2016-07-08', '10008',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('72634', 'Bawang putih', 4,
'siung', 10000, '2016-07-08', '10006',
'16906');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('79276', 'Ayam', 7,
'ekor', 25000, '2016-07-12', '10006',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('79817', 'Alpukat', 5,
'buah', 8000, '2016-07-12', '10004',
'16906');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('73142', 'Bebek', 1,
'ekor', 32000, '2016-07-12', '10002',
'16907');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('73001', 'Kopi', 400,
'gram', 43, '2016-07-12', '10001',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('71550', 'Kopi', 500,
'gram', 40, '2016-07-25', '10011',
'16907');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('71706', 'Teh', 10,
'kantong', 1500, '2016-07-25', '10011',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('77651', 'Teh', 6,
'kantong', 1500, '2016-08-01', '10011',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('73584', 'Telur', 7,
'butir', 2000, '2016-08-01', '10011',
'16911');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('72794', 'Cumi', 5,
'ons', 8000, '2016-08-01', '10009',
'16906');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('73528', 'Beras', 1500,
'gram', 77, '2016-08-06', '10008',
'16911');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('74541', 'Garam', 2000,
'gram', 50, '2016-08-07', '10008',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('73414', 'Gula', 800,
'gram', 50, '2016-08-15', '10008',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('72131', 'Kopi', 750,
'gram', 37, '2016-08-15', '10008',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('75788', 'Ayam', 5,
'ekor', 25000, '2016-08-15', '10007',
'16906');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('77335', 'Alpukat', 8,
'buah', 8000, '2016-08-23', '10004',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('76759', 'Apel', 5,
'buah', 5000, '2016-08-23', '10004',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('79344', 'Bebek', 9,
'ekor', 35000, '2016-08-23', '10003',
'16907');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('78898', 'Bebek', 3,
'ekor', 35000, '2016-08-23', '10002',
'16907');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('75689', 'Air', 1500,
'ml', 30, '2016-08-29', '10001',
'16911');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('72816', 'Kailan', 8,
'ikat', 10000, '2016-08-29', '10001',
'16911');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('78247', 'Keju', 500,
'gram', 40, '2016-09-01', '10001',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('75437', 'Teh', 30,
'kantong', 1500, '2016-09-01', '10001',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('78627', 'Tepung', 500,
'gram', 16, '2016-09-01', '10001',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('73460', 'Cumi', 5,
'ons', 8400, '2016-09-01', '10009',
'16906');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('79027', 'Gurame', 3,
'ekor', 15000, '2016-09-01', '10009',
'16907');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('71043', 'Tepung', 2000,
'gram', 8, '2016-09-02', '10008',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('71838', 'Telur', 20,
'butir', 2000, '2016-09-02', '10001',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('73138', 'Gurame', 10,
'ekor', 15000, '2016-09-02', '10010',
'16905');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('77263', 'Apel', 10,
'buah', 5000, '2016-09-02', '10005',
'16906');

INSERT INTO PEMBELIAN_BAHAN_BAKU(
nota, nama_bahan, jumlah, satuan,
harga_satuan, waktu, id_supplier,
id_karyawan)
VALUES('74544', 'Gurame', 8,
'ekor', 15000, '2016-09-03', '10010',
'16905');


QUERY:

1
Tampilkan nama karyawan dan nama bahan yang pernah dibeli oleh karyawan tersebut.

SELECT DISTINCT K.nama, P.nama_bahan
FROM KARYAWAN K
JOIN PEMBELIAN_BAHAN_BAKU P
ON K.id = P.id_karyawan
ORDER BY K.nama ASC;

2
Tampilkan semua nama supplier beserta jumlah penghasilannya dari pembelian bahan baku.

SELECT S.nama, SUM(P.jumlah * P.harga_satuan)
FROM SUPPLIER S
LEFT OUTER JOIN PEMBELIAN_BAHAN_BAKU P
ON S.id = P.id_supplier
GROUP BY nama;

3
Tampilkan nama karyawan bukan koki yang belum pernah melakukan pembelian bahan baku.

SELECT K.nama
FROM KARYAWAN K
WHERE NOT EXISTS (SELECT *
FROM KOKI KO
WHERE K.id = KO.id)
AND K.id NOT IN (SELECT P.id_karyawan
FROM PEMBELIAN_BAHAN_BAKU P);

4
Tampilkan nama koki yang pernah memasak menu �Bebek Goreng Mentega�.

SELECT DISTINCT K.nama
FROM KOKI KO, KARYAWAN K, MENU_HARIAN M
WHERE M.nama_menu = 'Bebek goreng mentega'
AND KO.id = K.id
AND M.id_koki = KO.id
ORDER BY K.nama;

5
Tampilkan nama menu beserta kategorinya yang hanya pernah dibuat pada bulan Juli 2016.

SELECT DISTINCT M.nama, K.nama
FROM MENU M, KATEGORI K, MENU_HARIAN MH
WHERE M.id_kategori = K.id
AND M.nama = MH.nama_menu
AND EXTRACT(Month FROM MH.tanggal)='7'
AND EXTRACT(YEAR FROM MH.tanggal)='2016'
ORDER BY M.nama ASC;

6
Tampilkan nama menu yang mengandung bahan baku �AIR� yang pernah dimasak oleh koki yang bernama �Hermione Ferguson�.

SELECT M.nama
FROM MENU M, MENU_HARIAN MH, KOKI KO,
BAHAN_BAKU_MENU BBM, KARYAWAN K
WHERE M.nama = BBM.nama_menu
AND BBM.nama_bahan = 'Air'
AND M.nama = MH.nama_menu
AND MH.id_koki = KO.id
AND KO.id = K.id
AND K.nama = 'Hermione Ferguson';

7
Tampilkan nama menu yang memerlukan bahan baku tepat 3 jenis.

SELECT nama_menu
FROM (SELECT nama_menu, COUNT(*) AS counter
FROM BAHAN_BAKU_MENU
GROUP BY nama_menu) AS foo
WHERE counter = 3
ORDER BY nama_menu ASC;

8
Tampilkan nama, tanggal lahir, posisi dari karyawan yang No KTP nya berawalan �9�.

SELECT nama, tgl_lahir, posisi
FROM KARYAWAN
WHERE no_KTP LIKE '9%';

9
Tampilkan nama supplier dan nomor telp nya yang pernah mensupply hanya 1 jenis bahan.

SELECT S.nama, S.telepon
FROM SUPPLIER S, (SELECT *
FROM (SELECT PBB.id_supplier, COUNT(*) AS counter
FROM PEMBELIAN_BAHAN_BAKU PBB
GROUP BY id_supplier) AS foo
WHERE counter = 1) AS foo
WHERE S.id = foo.id_supplier;

10
Tampilkan nama supplier yang tidak mempunyai email yang pernah mensupply (Telur dan Tepung) atau gurame.

SELECT nama
FROM SUPPLIER,
(((SELECT id_supplier
FROM PEMBELIAN_BAHAN_BAKU
WHERE nama_bahan = 'Telur')
INTERSECT
(SELECT id_supplier
FROM PEMBELIAN_BAHAN_BAKU
WHERE nama_bahan = 'Tepung'))
UNION
(SELECT id_supplier
FROM PEMBELIAN_BAHAN_BAKU
WHERE nama_bahan = 'Gurame')) AS foo
WHERE email IS NULL
AND id = foo.id_supplier;

11
Tampilkan total biaya yang harus dikeluarkan untuk membuat bubur ayam.

SELECT SUM(foo.jumlah * foo.harga_satuan)
AS total_biaya
FROM 
(SELECT DISTINCT BM.jumlah,
P.harga_satuan
FROM BAHAN_BAKU_MENU BM
JOIN PEMBELIAN_BAHAN_BAKU P
ON BM.nama_bahan = P.nama_bahan
WHERE BM.nama_menu = 'Bubur ayam') AS foo;

12
Tampilkan total harga terbesar pembelian bahan baku beserta nomor nota, nama bahan, dan nama suppliernya.

SELECT foo.harga,
P.nota, P.nama_bahan, S.nama
FROM PEMBELIAN_BAHAN_BAKU P, SUPPLIER S,
(SELECT MAX(jumlah * harga_satuan) AS harga
FROM PEMBELIAN_BAHAN_BAKU) AS foo
WHERE S.id = P.id_supplier
AND P.jumlah * P.harga_satuan = foo.harga;

13
Tampilkan nama koki yang memasak lebih dari 1 menu di hari yang sama terurut berdasarkan abjad.

SELECT DISTINCT K.nama
FROM KARYAWAN K, KOKI KO,
(SELECT tanggal, id_koki, count(*) AS counter
FROM MENU_HARIAN
GROUP BY tanggal, id_koki) AS foo
WHERE KO.id = K.id
AND KO.id = foo.id_koki
AND counter > 1
ORDER BY K.nama ASC;

14
Tampilkan nama kategori beserta jumlah jenis menu yang termasuk kategorinya.

SELECT K.nama, COUNT(M.nama) AS jumlah_menu
FROM KATEGORI K, MENU M
WHERE K.id = M.id_kategori
GROUP BY K.nama;

15
Tampilkan nama supplier yang tidak mempunyai email dan yang melayani pembelian bahan baku tepat 1 kali.

SELECT S.nama
FROM SUPPLIER S,
(SELECT id_supplier, COUNT(*) AS counter
FROM PEMBELIAN_BAHAN_BAKU
GROUP BY id_supplier) AS foo
WHERE S.email IS NULL
AND counter = 1
AND S.id = foo.id_supplier;
