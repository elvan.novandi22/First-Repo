import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class PrimeCheckerTest {

    private static final List<Integer> PRIME_NUMBERS = Arrays.asList(2, 3, 5, 7);
    private static final List<Integer> NONPRIME_NUMBERS = Arrays.asList(1, 4, 6, 8, 9);
    private PrimeChecker passer;

    @Test
    public void testIsPrimeTrueGivenPrimeNumbers() {
        PRIME_NUMBERS.forEach(number -> assertTrue(PrimeChecker.isPrime(number)));
        passer = new PrimeChecker();
    }

    @Test
    public void testIsPrimeFalseGivenNonPrimeNumbers() {
        NONPRIME_NUMBERS.forEach(number -> assertFalse(PrimeChecker.isPrime(number)));
    }

    @Test
    public void testMain() {
        System.out.println("main");
        final InputStream original = System.in;
        PrimeChecker.main(null);
        System.setIn(original);
    }
}
