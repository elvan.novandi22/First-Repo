import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class WordCountTest {

    private static final List<String> LINES = Arrays.asList("lorem", "ipsum",
            "dolor", "sit", "amet", "lorem", "ipsum");
    private Path testFixtureFile;
    private WordCount passer;

    @Before
    public void setUp() throws Exception {
        testFixtureFile = Files.createTempFile("testWordCount", "txt");
        Files.write(testFixtureFile, LINES, Charset.defaultCharset());
        passer = new WordCount();
    }

    @Test
    public void testExistingWordsShouldCountedCorrectly() throws Exception {
        assertEquals(2, WordCount.countLines("lorem", testFixtureFile));
    }

    @Test
    public void testExistingSubstringShouldCountedCorrectly() throws Exception {
        assertEquals(3, WordCount.countLines("lor", testFixtureFile));
    }

    @Test
    public void testNonExistingWordsShouldCountedCorrectly() throws Exception {
        assertEquals(0, WordCount.countLines("kolor", testFixtureFile));
    }

    @Test
    public void testMain() {
        System.out.println("main");
        final InputStream original = System.in;
        ScoreGrouping.main(null);
        System.setIn(original);
    }

    @After
    public void tearDown() throws Exception {
        Files.delete(testFixtureFile);
    }
}
