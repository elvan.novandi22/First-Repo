package sorting;

public class Finder {


    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     * @param arrOfInt is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int slowSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int element : arrOfInt) {
            if (element == searchedValue) {
                returnValue = element;
            }
        }

        return returnValue;
    }

    /**
     * algoritma searchnya lebih enhanced
     * dengan param dan return yang sama
     */
    public static int slowSearchEnhanced(int[] arrOfInt, int searchedInt) {
        for (int element : arrOfInt) {
            if (element == searchedInt) {
                return element;
            }
        }

        return -1;
    }

    /**
     * binary search untuk array yang telah sorted
     * param array dan angka yang dicari
     * return angka yang dicari atau -1 jika tidak ada
     */
    public static int binarySearch(int[] arrayInt, int searchInt) {
        int panjangArray = arrayInt.length - 1;
        return binarySearchHelper(arrayInt, searchInt, 0, panjangArray);
    }

    public static int binarySearchHelper(int[] array, int search, int awal, int akhir) {
        if (akhir >= 1 && (	akhir > awal)) {
            int tengah = awal + (akhir - awal) / 2;
            if (array[tengah] == search) {
                return search;
            } else if (array[tengah] > search) {
                return binarySearchHelper(array, search, awal, tengah - 1);
            } else {
                return binarySearchHelper(array, search, tengah + 1, akhir);
            }
        }
        return -1;
    }
}
