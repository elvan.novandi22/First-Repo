package tutorial.javari;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

public class JavariDatabase {
    private List<Animal> listAnimal;
    private final String dataPath = "animal_data.csv";
    private final Path file = Paths.get("", dataPath);

    public JavariDatabase() throws IOException {
        listAnimal = new ArrayList<>();
        this.loadingAllData();
    }

    public List<Animal> getListAnimals() throws IOException {
        this.listAnimal.clear();
        this.loadingAllData();
        return this.listAnimal;
    }

    public Animal getAnimalWithId(int id) throws IOException {
        this.listAnimal.clear();
        this.loadingAllData();
        for (Animal animal : listAnimal) {
            if (id == animal.getId()) {
                return animal;
            }
        }
        return null;
    }

    public Animal deleteAnimalWithId(int id) throws IOException {
        Animal animal = null;
        for (int j = 0; j < listAnimal.size(); j++) {
            if (listAnimal.get(j).getId() == id) {
                animal = listAnimal.remove(j);
                break;
            }
        }
        this.savingtheData();
        return animal;
    }

    public Animal addNewAnimal(String animal) throws IOException, JSONException {
        Animal animalNew = this.convertJsonIntoAnimal(animal);
        if (!this.cekIfDuplicate(animalNew)) {
            listAnimal.add(animalNew);
            this.savingtheData();
            return animalNew;
        }
        return null;
    }


    // Method only for this database //

    private void loadingAllData() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file.toString()));
        String readLine = reader.readLine();

        while (readLine != null) {
            if (readLine.equals("")) {
                break;
            }
            this.listAnimal.add(convertInputToAnimal(readLine));
            readLine = reader.readLine();
        }

        reader.close();
    }

    private void savingtheData() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file.toString()));
        for (Animal iterAnimal : listAnimal) {
            String output = this.animalToOutputFile(iterAnimal);
            writer.write(output);
            writer.newLine();
        }
        writer.close();
    }

    private boolean cekIfDuplicate(Animal animal) {
        for (Animal locAnimal : listAnimal) {
            if (animal.getId() == locAnimal.getId()) {
                return true;
            }
        }
        return false;
    }

    private String animalToOutputFile(Animal animal) {
        String[] component = {animal.getId().toString(), animal.getName(),
                animal.getType(), animal.getGender().toString(),
                String.valueOf(animal.getLength()),
                String.valueOf(animal.getWeight()),
                animal.getCondition().toString()
        };
        return String.join(",", component);
    }

    private Animal convertJsonIntoAnimal(String input) throws JSONException {
        JSONObject json = new JSONObject(input);
        return new Animal(json.getInt("id"), json.getString("type"),
                json.getString("name"), Gender.parseGender(json.getString("gender")),
                json.getDouble("length"), json.getDouble("weight"),
                Condition.parseCondition(json.getString("condition")));
    }

    private Animal convertInputToAnimal(String fileInput) {
        String[] listInput = fileInput.split(",");
        Integer id = Integer.parseInt(listInput[0]);
        String typeAnimal = listInput[1];
        String nameAnimal = listInput[2];
        Double lengthAnimal = Double.parseDouble(listInput[4]);
        Double weightAnimal = Double.parseDouble(listInput[5]);
        return new Animal(id, typeAnimal,
                nameAnimal,
                Gender.parseGender(listInput[3]),
                lengthAnimal, weightAnimal,
                Condition.parseCondition(listInput[6])
        );
    }
}
