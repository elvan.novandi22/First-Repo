package tutorial.javari;

public class Message {
    private String messageType;
    private String message;

    private Message(String messageType, String message) {
        this.messageType = messageType;
        this.message = message;
    }

    public static Message notFoundMessage(int idAnimal) {
        return new Message("Unknown",
                "Sorry, there is no animal with id no." + idAnimal);
    }

    public static Message nothingInDatabase() {
        return new Message("Unknown",
                "Sorry, our database is empty");
    }

    public static Message duplicateMessage() {
        return new Message("Caution",
                "Sorry, it seems there's already animal with "
                        + "same ID in the database");
    }

    public static Message successDeleteAnimal() {
        return new Message("Success",
                "The animal is successfully deleted from the database."
                        + "This is the data from the animal that just got deleted:");
    }

    public static Message successAddAnimal() {
        return new Message("Success",
                "The animal successfully added to the database."
                        + "This is the data from the animal that just got added:");
    }

    public String getMessageType() {
        return messageType;
    }

    public String getMessage() {
        return message;
    }
}
