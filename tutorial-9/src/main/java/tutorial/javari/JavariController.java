package tutorial.javari;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tutorial.javari.animal.Animal;
import tutorial.javari.JavariDatabase;
import tutorial.javari.Message;

@RestController
public class JavariController {
    JavariDatabase database = new JavariDatabase();

    public JavariController() throws IOException {
    }

    @GetMapping("/javari")
    public Object getAllAnimal() throws IOException {
        List<Animal> listAnimal = database.getListAnimals();
        if (listAnimal == null || listAnimal.isEmpty()) {
            return Message.nothingInDatabase();
        }
        return listAnimal;
    }

    @GetMapping("/javari/{id}")
    public Object getAnimalwithId(@PathVariable Integer id) throws IOException {
        Animal animal = database.getAnimalWithId(id);
        if (animal == null) {
            return Message.notFoundMessage(id);
        }
        return animal;
    }

    @DeleteMapping("/javari/{id}")
    public Object deleteAnimal(@PathVariable Integer id) throws IOException {
        Animal animal = database.deleteAnimalWithId(id);
        if (animal != null) {
            Object[] messageAnimal = {Message.successDeleteAnimal(), animal};
            return messageAnimal;
        }
        return Message.notFoundMessage(id);
    }

    @PostMapping("/javari")
    public Object insertAnimal(@RequestBody String input) throws IOException, JSONException {
        Animal animal = database.addNewAnimal(input);
        Message message;
        if (animal != null) {
            message = Message.successAddAnimal();
            Object[] animalMessage = {message, animal};
            return animalMessage;
        }
        message = Message.duplicateMessage();
        return message;
    }
}
