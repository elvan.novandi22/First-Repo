package tutorial.javari;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tutorial.javari.animal.Animal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavariControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private JavariDatabase database;
    private List<String> listAnimal;

    @Before

    public void setUp() throws IOException,JSONException {
        ObjectMapper mapper = new ObjectMapper();
        database = new JavariDatabase();
        listAnimal = new ArrayList<>();
        database.getListAnimals().forEach(
                i -> {
                    try {
                        listAnimal.add(mapper.writeValueAsString(i));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                });
    }

    @Test
    public void getAnimalwithIdTest() throws Exception {
        this.mockMvc.perform(get("/javari/1"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.id")
                        .value("1"));
    }

    @Test
    public void getNotFoundIdTest() throws Exception {
        this.mockMvc.perform(get("/javari/300"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.messageType")
                        .value("Unknown"));
    }

    @Test
    public void deleteWithNotFoundIdTest() throws Exception {
        this.mockMvc.perform(delete("/javari/999"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.messageType")
                        .value("Unknown"));
    }

    @Test
    public void getListAllAnimalsTest() throws Exception {
        this.mockMvc.perform(get("/javari/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[1].id")
                        .value("2"));
    }

    /*
    Because the test affects the database, we need to do delete and add sequentially
    In this case, delete the existing data and then add it back
     */
    @Test
    public void deleteAddEmptyTest() throws Exception {
        deleteIdAnimalTest();
        emptyAllAnimalDataTest();
        addAnAnimalTest();
        addDuplicateIdTest();

    }

    public void deleteIdAnimalTest() throws Exception {
        for (Animal animal : database.getListAnimals()) {
            int idAnimal = animal.getId();
            this.mockMvc.perform(delete("/javari/" + idAnimal))
                    .andDo(print()).andExpect(status().isOk())
                    .andExpect(jsonPath("$[0].messageType")
                            .value("Success"))
                    .andExpect(jsonPath("$[1].id").value(idAnimal));

            this.mockMvc.perform(get("/javari/" + idAnimal))
                    .andDo(print()).andExpect(status().isOk())
                    .andExpect(jsonPath("$.messageType")
                            .value("Unknown"));
        }
    }

    public void emptyAllAnimalDataTest() throws Exception {
        this.mockMvc.perform(get("/javari"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.messageType")
                        .value("Unknown"));
    }

    public void addAnAnimalTest() throws Exception {
        int pointer = 0;
        for (Animal animal : database.getListAnimals()) {
            int idAnimal = animal.getId();
            this.mockMvc.perform(post("/javari").content(listAnimal.get(pointer)))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$[0].messageType").value("Success"))
                    .andExpect(jsonPath("$[1].id").value(idAnimal));

            this.mockMvc.perform(get("/javari/" + idAnimal))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").value(idAnimal));
            pointer++;
        }
    }

    public void addDuplicateIdTest() throws Exception {
        this.mockMvc.perform(post("/javari").content(listAnimal.get(0)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.messageType")
                        .value("Caution"));
    }
}
