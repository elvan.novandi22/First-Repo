package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ClamPizzaTest {
    final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private Pizza clamPizza;

    @Before
    public void setUp() {
        clamPizza = new ClamPizza(new NewYorkPizzaIngredientFactory());
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void testPrepare() {
        clamPizza.setName("Clam Pizza Tester");
        clamPizza.prepare();
        assertEquals("Preparing Clam Pizza Tester"
            + System.getProperty("line.separator"), outContent.toString());
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }
}
