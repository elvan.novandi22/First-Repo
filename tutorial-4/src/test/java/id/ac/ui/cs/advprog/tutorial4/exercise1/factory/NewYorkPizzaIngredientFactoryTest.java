package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaIngredientFactoryTest {
    private NewYorkPizzaIngredientFactory nyFactory;

    @Before
    public void setUp() {
        nyFactory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testCreateDough() {
        assertEquals(ThinCrustDough.class, nyFactory.createDough().getClass());
    }

    @Test
    public void testCreateSauce() {
        assertEquals(MarinaraSauce.class, nyFactory.createSauce().getClass());
    }

    @Test
    public void testCreateCheese() {
        assertEquals(ReggianoCheese.class, nyFactory.createCheese().getClass());
    }

    @Test
    public void testCreateClam() {
        assertEquals(FreshClams.class, nyFactory.createClam().getClass());
    }

    @Test
    public void testCreateVeggies() {
        Veggies[] veggies = {new Garlic(), new Onion(), new Mushroom(), new RedPepper()};
        for (int i = 0; i < veggies.length; i++) {
            assertEquals(veggies[i].getClass(), nyFactory.createVeggies()[i].getClass());
        }
    }
}
