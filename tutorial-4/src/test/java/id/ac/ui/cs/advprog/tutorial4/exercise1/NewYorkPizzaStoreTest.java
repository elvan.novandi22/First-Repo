package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    private NewYorkPizzaStore nyStore;

    @Before
    public void setUp() {
        nyStore = new NewYorkPizzaStore();
    }

    @Test
    public void testCheese() {
        Pizza pizza = nyStore.createPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza.getName());
        assertEquals(CheesePizza.class, pizza.getClass());
    }

    @Test
    public void testVeggie() {
        Pizza pizza = nyStore.createPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza.getName());
        assertEquals(VeggiePizza.class, pizza.getClass());
    }

    @Test
    public void testClam() {
        Pizza pizza = nyStore.createPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza.getName());
        assertEquals(ClamPizza.class, pizza.getClass());
    }
}
