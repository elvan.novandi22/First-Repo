package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class LettuceTest {
    private Lettuce lettuce;

    @Before
    public void setUp() {
        lettuce = new Lettuce();
    }

    @Test
    public void testToString() {
        assertEquals("Lettuce", lettuce.toString());
    }
}
