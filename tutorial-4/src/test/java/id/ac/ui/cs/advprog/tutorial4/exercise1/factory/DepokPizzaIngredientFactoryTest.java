package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Lettuce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {
    private DepokPizzaIngredientFactory dpkFactory;

    @Before
    public void setUp() {
        dpkFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testCreateDough() {
        assertEquals(ThickCrustDough.class, dpkFactory.createDough().getClass());
    }

    @Test
    public void testCreateSauce() {
        assertEquals(PlumTomatoSauce.class, dpkFactory.createSauce().getClass());
    }

    @Test
    public void testCreateCheese() {
        assertEquals(MozzarellaCheese.class, dpkFactory.createCheese().getClass());
    }

    @Test
    public void testCreateClam() {
        assertEquals(FrozenClams.class, dpkFactory.createClam().getClass());
    }

    @Test
    public void testCreateVeggies() {
        Veggies[] veggies = {new Lettuce(), new Eggplant(), new BlackOlives(), new Spinach()};
        for (int i = 0; i < veggies.length; i++) {
            assertEquals(veggies[i].getClass(), dpkFactory.createVeggies()[i].getClass());
        }
    }
}
