package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Before;
import org.junit.Test;

public class PizzaStoreTest {
    private PizzaStore nyStore;

    @Before
    public void setUp() {
        nyStore = new NewYorkPizzaStore();
    }

    @Test
    public void testOrderPizza() {
        Pizza pizza = nyStore.orderPizza("cheese");
        assertEquals(CheesePizza.class, pizza.getClass());
    }
}
