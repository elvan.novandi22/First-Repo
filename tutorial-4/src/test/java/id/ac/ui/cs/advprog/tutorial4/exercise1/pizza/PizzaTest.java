package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.HardClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PizzaTest {
    final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private Pizza cheesePizza;

    @Before
    public void setUp() {
        cheesePizza = new CheesePizza(new NewYorkPizzaIngredientFactory());
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void testBake() {
        cheesePizza.bake();
        assertEquals("Bake for 25 minutes at 350"
            + System.getProperty("line.separator"), outContent.toString());
    }

    @Test
    public void testCut() {
        cheesePizza.cut();
        assertEquals("Cutting the pizza into diagonal slices"
            + System.getProperty("line.separator"), outContent.toString());
    }

    @Test
    public void testBox() {
        cheesePizza.box();
        assertEquals("Place pizza in official PizzaStore box"
            + System.getProperty("line.separator"), outContent.toString());
    }

    @Test
    public void testName() {
        cheesePizza.setName("test");
        assertEquals("test", cheesePizza.getName());
    }

    @Test
    public void testToString() {
        cheesePizza.clam = new HardClams();
        cheesePizza.veggies = new Veggies[]{new Onion(), new Mushroom()};
        cheesePizza.setName("Depok Cheese Pizza Test");
        cheesePizza.prepare();
        assertEquals("---- Depok Cheese Pizza Test ----\n"
            + "Thin Crust Dough\n"
            + "Marinara Sauce\n"
            + "Reggiano Cheese\n"
            + "Onion, Mushrooms\n"
            + "Hard Clams from Northern Quahog\n", cheesePizza.toString());
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }

}
