package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BarbequeSauceTest {
    private BarbequeSauce bbqSauce;

    @Before
    public void setUp() {
        bbqSauce = new BarbequeSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Barbeque Sauce", bbqSauce.toString());
    }
}
