package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private DepokPizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testCheese() {
        Pizza pizza = depokPizzaStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());
        assertEquals(CheesePizza.class, pizza.getClass());
    }

    @Test
    public void testVeggie() {
        Pizza pizza = depokPizzaStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza.getName());
        assertEquals(VeggiePizza.class, pizza.getClass());
    }

    @Test
    public void testClam() {
        Pizza pizza = depokPizzaStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza.getName());
        assertEquals(ClamPizza.class, pizza.getClass());
    }
}
