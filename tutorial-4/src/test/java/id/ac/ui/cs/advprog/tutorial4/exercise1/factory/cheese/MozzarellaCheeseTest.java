package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MozzarellaCheeseTest {
    private MozzarellaCheese MozzarellaCheese;

    @Before
    public void setUp() {
        MozzarellaCheese = new MozzarellaCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Shredded Mozzarella", MozzarellaCheese.toString());
    }
}
